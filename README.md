# Keid Engine - Overview

This is not the minimal example possible, but an overview of major engine parts.

> It may help to check out the [project page] first, for more context.

> For an in-depth tutorial on how to set everything up yourself consult <https://vkguide.dev/>.
> It is coded in C++, but the basics are straightforward enough to write in Haskell.

[project page]: https://gitlab.com/keid

[[_TOC_]]

The code will result in a simple spinning animation for procedurally-generated spheres.

![2021-02-20](https://i.imgur.com/kgzGWk0.png)

```haskell
module Main (main) where
```

This will be a single-module executable.
In real projects, however, Main should contain only the minimal boilerplate
and everything else should be neatly stowed in modules.
This could come in handy later for running parts of the project and even live-reloading.

## Imports

The project uses RIO prelude both for its `RIO` type and a bunch of common re-exports.

```haskell
import RIO
```

Writing this tutorial prompted me to move unnecessary boilerplate into `Engine.App`.

```haskell
import Engine.App (engineMain)
```

That should be enough to kickstart your code.

> This tutorial will have everything in one file, so the imports continue.

```haskell
import Engine.Types qualified as Engine
```

### Rendering

A major theme of this playground-the-engine is «stages».
A stage is like a mini-application with its own resources, event handlers, and rendering loop.
Multiple stages can be stacked on top of each other, but only the topmost will have its rendering loop active.

Since a stage is a first and foremost a rendering abstraction, it will need some Vulkan-related imports.

```haskell
import Engine.Vulkan.Swapchain (setDynamicFullscreen)
import Engine.Vulkan.Types (Queues)
```

The engine provides a simple universal render pass.
It has antialiasing and is suitable for both 2D and 3D rendering.

```haskell
import Render.Basic qualified as Basic
import Render.ForwardMsaa qualified as ForwardMsaa
```

Next, we'll need pipelines.
Those are the things that do the actual rendering.


```haskell
import Render.Unlit.Colored.Model qualified as UnlitColored
```

Engine-provided pipelines are built to have their data in a very specific layout.
The engine provides one for typical drawing tasks so the pipelines can be used right away.

```haskell
import Render.DescSets.Set0 qualified as Set0
```

### Resources

The stage in this tutorial will load its model resource itself.
Real stages should delegate that away and spend only minimal effort here to avoid inter-stage freezes.

```haskell
import Engine.Camera qualified as Camera
import Engine.Worker qualified as Worker
import Resource.Buffer qualified as Buffer
import Resource.CommandBuffer (allocatePools)
import Resource.Model qualified as Model
```

Additionally, extra imports are needed to spell out types for top-level definitions and use resources.

```haskell
import Control.Monad.Trans.Resource (ResourceT)
import RIO.Vector.Storable qualified as Storable
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk
```

And some rendering helpers.

```haskell
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Geomancy (Transform, Vec3, vec3)
import Geomancy.Transform qualified as Transform
import Geomancy.Vec3 qualified as Vec3
import Geomancy.Vec4 qualified as Vec4
import Geomancy.Vulkan.View (lookAt)
import Geometry.Icosphere qualified as Icosphere
import Render.Draw qualified as Draw
import RIO.State (gets)
```

## Executable entry point

The whole application runs in a RIO environment and `engineMain` does the initial preparations like parsing command-line options and opening a window.

```haskell
main :: IO ()
main = engineMain mainStackStage
```

## Stage setup

An actual stage requires a wrapper that will hide stage implementation details from the rest of the stack.

```haskell
mainStackStage :: Engine.StackStage
mainStackStage = Engine.StackStage mainStage
```

Those details are encoded in Stage type parameters.
`Basic.Stage` is a type alias provided by `keid-render-basic` that fills in render pass and pipeline type so you don't have to.

```haskell
mainStage :: Basic.Stage FrameResources MainState
```

The stage type parameters describe the rendering setup for a compiler to prevent a square peg from accidentally going into a round hole.

In larger projects, the type can be shared between multiple stages.
This allows stages to pass compatible resources around.

### Stage template

It consists of a few sections related to a stage lifecycle:

- General information.
- Initialization.
- Render loop.
- Termination.

Loop procedures (update, record) don't have access to the data passed from «before loop» to «after loop» procedures.
This is to prevent accidental resource mishandling since a resource participating in a frame can still be in use when it is time for the stage to go away.
You can start some processes there, whose lifetimes are tied to that of the stage: event handlers, timers, etc.

An active render loop can be signaled to finish from inside the stage.
This example is single-stage and has no input events, so the `Main` stage will run forever.

```haskell
mainStage = Engine.Stage
  { sTitle = "Main"

  , sAllocateRP = Basic.allocate_
  , sAllocateP  = Basic.allocatePipelines_
  , sInitialRS  = initialRunState
  , sInitialRR  = intialRecyclableResources

  , sBeforeLoop     = pure ()
  , sUpdateBuffers  = updateBuffers
  , sRecordCommands = recordCommands
  , sAfterLoop      = \() -> pure ()
  }
```

> This snippet displays how the Stage objects is made on a low-level.
> In real projects you should use `Engine.Stage.Component.assemble` function and related types: `Rendering`, `Resources`, and `Scene`.

<!-- TODO: Rewrite this part using components. -->

### Stage data

A stage keeps its data in the state part of the RIO app.
The data stays in one place between render loop iterations.

This tutorial features two stage-bound resources:

- Processing thread that updates projection matrices in scene parameters in response to window resize events.
- Procedurally-generated model deployed to a fast part of GPU memory.

```haskell
data MainState = MainState
  { msSceneP :: Set0.Process
  , msModel  :: UnlitColored.Model 'Buffer.Staged
  }
```

> The engine distinguishes two types of buffers:
>
> - **Staged** buffers will be loaded into a fast GPU memory that is unavailable to CPU.
>   The host memory is then released.
> - **Coherent** buffers will reside on the CPU side.
>   Vulkan driver will then peek into it to see if it needs to take a snapshot into GPU.
>   It is a bit slower but allows faster updates, including size adjustments.

Naturally, this is exactly the place to produce and store static data.

```haskell
initialRunState :: Engine.StageRIO st (Resource.ReleaseKey, MainState)
initialRunState = do
```

Note the absence of `ResourceT`. The resource scope is *global* here.
Resources will be freed, but only at the last moment when the engine itself is shutting down.

You have to collect release keys and put them into an aggregated key.
The stack runner will release the resources by this key when the stage and its rendering are finished.
Don't worry about resources being released twice, the `resourcet` machinery makes this idempotent.

The engine runs window-tracking by itself, but you need to build a projection from it.

```haskell
  (perspectiveKey, perspective) <- Worker.registered Camera.spawnPerspective
```

An additional process will be spawned to assemble a `Scene` value and tag it with a version.
The calculation may be costly (it will get its own thread) but must be pure (it will run inside STM).
Any buffer wrangling has to be deferred to `updateBuffers`.

> A Merge is a process that will wait until at least one of its inputs have incremented their version.
> Its function will be run and its output version incremented.
> That may prompt updates in some other processes.

A process should be finished at some time and its key is used to tie that time to the stage.

```haskell
  let staticView = lookAt 0 (vec3 0 0 1) (vec3 0 (-1) 0)
  (sceneKey, msSceneP) <- Worker.registered $
    Worker.spawnMerge1 (mkScene staticView) perspective
```

Next, we'll do some GPU work.
This requires a Vulkan context object and a temporary command pool to submit one-shot transfer commands.

> Vulkan requires each thread to have its own pool, so you may need to allocate more pools if you plan to submit GPU jobs in parallel.

```haskell
  context <- ask
  (bqKey, pools) <- allocatePools context
```

A `Model` resource prepares vertex data, distributing it into multiple buffers.
The engine uses «positions/attributes/indices» partitioning to reuse position data and be polymorphic on attributes.

You can load such models from files or generate them on the fly.

```haskell
  let (positions, attrs, indices) = someModel
  msModel <- Model.createStaged context pools positions attrs indices
  modelKey <- Resource.register $
    Model.destroyIndexed context msModel
```

The temporary command pools are free to go now, assuming you waited on all the transfers to finish (which `Model` loaders do).

```haskell
  Resource.release bqKey
```

Finally, it is time to create a composite release key.
You may even throw in some IO action in there, e.g. debugging.

```haskell
  releaseKeys <- Resource.register $ traverse_ Resource.release
    [ perspectiveKey
    , sceneKey
    , modelKey
    ]
```

Stage state is handled by the engine stage runner.
It will create, recreate and release it as needed and make it available for rendering procedures.

```haskell
  pure (releaseKeys, MainState{..})
```

#### Stage data helpers

A `Scene` has more fields, but the pipeline used here only needs those two.

```haskell
mkScene :: Transform -> Camera.Projection 'Camera.Perspective -> Set0.Scene
mkScene staticView Camera.Projection{..} = Set0.emptyScene
  { Set0.sceneProjection = projectionTransform
  , Set0.sceneView       = staticView
  }
```

Let's produce a nicely subdivided sphere, colored by its normal direction.

```haskell
someModel
  :: ( Storable.Vector Vec3.Packed
     , Storable.Vector UnlitColored.VertexAttrs
     , Storable.Vector Word32
     )
someModel =
  Icosphere.generateIndexed
    3
    mkInitialAttrs
    mkMidpointAttrs
    mkVertices
  where
    mkInitialAttrs :: Vec3 -> ()
    mkInitialAttrs _pos = ()

    mkMidpointAttrs :: Float -> Vec3 -> () -> () -> ()
    mkMidpointAttrs _scale _midPos _attr1 _attr2 = ()

    mkVertices points _faces = do
      (rawPos, ()) <- points
      let normPos = Vec3.normalize rawPos
      pure
        ( Vec3.Packed normPos
        , Vec4.fromVec3 (normPos / 2 + 0.5) 1
        )
```

### Recyclable frame resources

⚠️ Data submitted to GPU for rendering should not be updated on the pain of major visual glitches or freezes.

To prepare data for the upcoming frame all the *mutable* data is duplicated like the framebuffers of ye olde days, swapping «in use» and «available» memory after each submission.

This data will be attached to the engine's own `RecycledResources` type.

Here, only the scene data will be updated.

```haskell
data FrameResources = FrameResources
  { frScene :: Set0.FrameResource '[Set0.Scene]
  }
```

Initialization for `RecycledResources` (including stage data) happens in a *Frame* scope.
Resources will be freed after the last frame of a stage will finish its rendering.
Before that happens, "used" stage resources will be repeatedly made available for upcoming frames.

```haskell
intialRecyclableResources
  :: Queues Vk.CommandPool
  -> Basic.RenderPasses
  -> Basic.Pipelines
  -> ResourceT (Engine.StageRIO MainState) FrameResources
intialRecyclableResources _cmdPools _renderPasses pipelines = do
```
The scene initializer knows how to create all three parts.

```haskell
  frScene <- Set0.allocateEmpty (Basic.getSceneLayout pipelines)
  pure FrameResources{..}
```

## Rendering

The frame rendering is split into two parts: buffer updates and command recording.
They run in the same environment, using the same stage parameters.

`Engine.StageFrameRIO` is another wrapper that provides access to a frame being processed/rendered.
That includes the rendering environment and resources that are guaranteed to be available for writing.
The price for this is doubled resource usage for all the changing things that go into rendering.

### Buffer updates

In this part, you check if the buffers that will be used by the rendering part are up to date.

You should avoid doing any processing here. All the calculations done here are essentially duplicated.

The good news is that the frame resources are recycled, and if something hasn't changed since the previous update, you can skip updating its content.

```haskell
updateBuffers
  :: MainState
  -> FrameResources
  -> Basic.StageFrameRIO FrameResources MainState ()
updateBuffers MainState{..} FrameResources{..} = do
```

The only thing to do for such a simple stage is to update scene data.
But only if it is changed (i.e the window is resized).

```haskell
  Set0.observe msSceneP frScene
```

### Drawing commands

The second part of render loop handling springs into action after a swapchain image is acquired.
At this moment a driver is expecting us to submit a command buffer for rendering and presentation.

```haskell
recordCommands
  :: Vk.CommandBuffer
  -> FrameResources
  -> Word32
  -> Basic.StageFrameRIO FrameResources MainState ()
recordCommands cb FrameResources{..} imageIndex = do
```

Fetch models to draw...

```haskell
  thingModel <- gets msModel
```

Fetch environment...

```haskell
  (context, Engine.Frame{fSwapchainResources, fRenderpass, fPipelines}) <- ask
  time <- getMonotonicTime
  let
    spin = Transform.rotateZ (realToFrac time)
    unspin = Transform.rotateZ (negate $ realToFrac time)
    thingTransforms = Storable.fromList
      [ spin <> Transform.translate 0 0 10    -- Move away from camera
      , spin <> Transform.translate 0 (-5) 10 -- Move away and *up*
      , spin <> Transform.translate 5 0 10    -- Move away and right
      , unspin <> Transform.translate 5 0 25 <> spin
      ]

  (_instanceKey, thingInstances) <- Buffer.allocateCoherent
    context
    Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
    10
    thingTransforms
```

----

> — Waaaait a minute! Aren't those some *calculations*?
> And buffer allocation you warned us not to do on hot path?
> And sampling time at some undefined moment?

Well, yes. They are.

Normally, you should keep this all out of `recordCommands` - in threads or at least in `updateBuffers`.
There are times, however, when you really need to try out just a few dirty hacks before committing them into types and stuff.

Since this is still a stage context, merely extended by `Frame` data, one can use the regular buffer functions.

Resources allocated here will be tied to a frame and released when the frame is presented.
Such ephemeral resources can make sense **if** they continue to come and go with each moment, despite the world being on pause and no input coming in.
Or, very quick and very dirty hacks.
I won't tell anyone, but a profiler can.

----

Finally, The Drawing Code! 🌋

1) Pick your canvas.
Since this is a screen pass, it needs to be adjusted to match the current screen size.

```haskell
  ForwardMsaa.usePass (Basic.rpForwardMsaa fRenderpass) imageIndex cb do
    setDynamicFullscreen cb fSwapchainResources
```

2) Pick your paints.
One of the pipelines will be used as a reference to check for compatibility with others.
Scene descriptors for the current frame will be used to provide data for all the pipelines in this block.

```haskell
    Set0.withBoundSet0 frScene (Basic.pUnlitColored fPipelines) cb do
```

3) Pick a brush that is compatible with both the paints and the canvas.

- ⚠️ It is an error to bind pipelines instantiated for different render passes.
- ⚠️ It is also an error to bind pipelines that expect different descriptor set layout.

```haskell
      Graphics.bind cb (Basic.pUnlitColored fPipelines) $
```

4) Draw some figures.

The engine provides some drawing procedures that check their environment to prevent drawing garbage or freezing.

`UnlitColored` pipeline uses the indexed «connect the dots» draws and `Draw.indexed` will put as many figures as there are instances (zero or more).

```haskell
        Draw.indexed cb thingModel thingInstances
```

### A brief FAQ concerning render loop handlers

- Q: *Is this the place where I run my input and/or timer handling?*
- A: No. You should run polls/callbacks/steps in separate threads.

- Q: *But why?*
- A1: Render loop is a «minimize latency» path.
  When the time comes to prepare the next frame your data should be processed for faster loading into buffers.
- A2: Frames made available to an application at some external rate.
  Using time intervals between two frames as an input for your game logic may lead to funky glitches.

- Q: *Okay, sure. Where do I start those separate threads?*
- A: `sBeforeLoop` handler of `Stage` value should be a good place to start worker threads.
  Use `sAfterLoop` to stop them right after a stage switch is triggered so they wouldn't run detached with no chance for anybody to see their updates.
